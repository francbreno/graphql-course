const { orderedFor, slug } = require("../lib/util");
const humps = require("humps");

module.exports = pgPool => {
  return {
    getUsersByIds(userIds) {
      return pgPool
        .query(
          `
        SELECT * FROM users
        WHERE id = ANY($1)
      `,
          [userIds]
        )
        .then(res => orderedFor(res.rows, userIds, "id"));
    },

    getUsersByApiKeys(apiKeys) {
      return pgPool
        .query(
          `
        SELECT * FROM users
        WHERE api_key = ANY($1)
      `,
          [apiKeys]
        )
        .then(res => orderedFor(res.rows, apiKeys, "apiKey"));
    },

    getContestsForUserIds(userIds) {
      return pgPool
        .query(
          `
      SELECT * FROM contests
      WHERE created_by = ANY($1)
      `,
          [userIds]
        )
        .then(res => orderedFor(res.rows, userIds, "createdBy", false));
    },

    getNamesForContestIds(contestIds) {
      return pgPool
        .query(
          `
        SELECT * FROM names
        WHERE contest_id = ANY($1)
      `,
          [contestIds]
        )
        .then(res => {
          return orderedFor(res.rows, contestIds, "contestId", false);
        });
    },

    getTotalVotesByNameIds(nameIds) {
      return pgPool
        .query(
          `
          select name_id, up, down from total_votes_by_name
          where name_id = ANY($1)
      `,
          [nameIds]
        )
        .then(res => {
          return orderedFor(res.rows, nameIds, "nameId");
        });
    },

    addContest({ apiKey, title, description }) {
      return pgPool
        .query(
          `
        INSERT INTO contests(code, title, description, created_by)
        VALUES ($1, $2, $3,
          (SELECT id FROM users WHERE api_key = $4)
        ) returning *
      `,
          [slug(title), title, description, apiKey]
        )
        .then(res => humps.camelizeKeys(res.rows[0]));
    },

    addName({ apiKey, contestId, label, description }) {
      return pgPool
        .query(
          `
        INSERT INTO names(label, normalized_label, description, contest_id, created_by)
        VALUES ($1, $2, $3, $4, (
          SELECT id FROM users WHERE api_key = $5)
        ) RETURNING *
      `,
          [label, humps.camelizeKeys(label), description, contestId, apiKey]
        )
        .then(res => humps.camelizeKeys(res.rows[0]));
    },

    getActivitiesForUserIds(userIds) {
      return pgPool
        .query(
          `
      SELECT created_by, created_at, label, '' as title,
             'name' as activity_type 
      FROM names
      WHERE created_by = ANY($1)
      UNION
      SELECT created_by, created_at, '' as label, title,
             'contest' as activity_type
      FROM contests
      WHERE created_by = ANY($1)
      `,
          [userIds]
        )
        .then(res => {
          return orderedFor(res.rows, userIds, "createdBy", false);
        });
    }
  };
};
