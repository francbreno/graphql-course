const humps = require("humps");
const groupBy = require("lodash.groupby");

module.exports = {
  nodeEnv: process.env.NODE_ENV || "development",
  orderedFor(rows, collection, field, singleObject = true) {
    const data = humps.camelizeKeys(rows);
    const inGroupsOfField = groupBy(data, field);
    return collection.map(element => {
      const elementArray = inGroupsOfField[element];
      if (elementArray) {
        return singleObject ? elementArray[0] : elementArray;
      }
      return singleObject ? {} : [];
    });
  },
  slug: str => str.toLowerCase().replace(/[\s\W-]+/, "-")
};
