const app = require("express")();
const graphqlHTTP = require("express-graphql");
const DataLoader = require("dataloader");
const pg = require("pg");
const { MongoClient } = require("mongodb");
const assert = require("assert");

const { nodeEnv } = require("./util");
const ncSchema = require("../schema");
const pgConfig = require("../config/pg")[nodeEnv];
const mongoConfig = require("../config/mongo")[nodeEnv];

const pgPool = new pg.Pool(pgConfig);
const pgdb = require("../database/pgdb")(pgPool);

MongoClient.connect(mongoConfig.url, (err, client) => {
  assert.equal(err, null);

  const mongoPool = client.db("contests");
  const mdb = require("../database/mdb")(mongoPool);

  // handle graphql requests: extract query, execute against the schema and respond back
  // to the caller
  app.use("/graphql", (req, res) => {
    const loaders = {
      usersByIds: new DataLoader(pgdb.getUsersByIds),
      usersByApiKeys: new DataLoader(pgdb.getUsersByApiKeys),
      namesForContestIds: new DataLoader(pgdb.getNamesForContestIds),
      contestsForUserIds: new DataLoader(pgdb.getContestsForUserIds),
      totalVotesByNameIds: new DataLoader(pgdb.getTotalVotesByNameIds),
      activitiesForUserIds: new DataLoader(pgdb.getActivitiesForUserIds),
      mdb: {
        usersByIds: new DataLoader(mdb.getUsersByIds)
      }
    };
    graphqlHTTP({
      schema: ncSchema,
      graphiql: true,
      context: {
        pgPool,
        mongoPool,
        loaders
      }
    })(req, res);
  });

  const PORT = process.env.PORT || 3000;
  app.listen(PORT, () => {
    console.log(`Server is up and running on port ${PORT}`);
  });
});
