const {
  GraphQLSchema, // To define a GraphQL Schema
  GraphQLObjectType, //
  GraphQLString, // To define a field of type String
  GraphQLNonNull // To define a required args for a field
} = require("graphql");

// const pgdb = require("../database/pgdb");

const UserType = require("./types/user");

// Define the root of the graph data. It defines the starting point
// on the data graph
const RootQueryType = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    user: {
      type: UserType,
      description: "The current user identified by an api key",
      args: {
        key: { type: new GraphQLNonNull(GraphQLString) }
      },
      // resolve: (obj, args, ctx) => {
      //   return pgdb(ctx.pgPool).getUserByApiKey(args.key);
      resolve: (obj, args, { loaders }) => {
        return loaders.usersByApiKeys.load(args.key);
      }
    },
    hello: {
      type: GraphQLString,
      description: "The *mandatory* hello world example, GraphQL style",
      resolve: () => "world!"
    }
  }
});

const AddContestMutation = require("./mutations/add-contest");
const AddNameMutation = require("./mutations/add-name");

const RootMutation = new GraphQLObjectType({
  name: "RootMutation",

  fields: () => ({
    AddContest: AddContestMutation,
    AddName: AddNameMutation
  })
});

const ncSchema = new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutation
});

module.exports = ncSchema;
