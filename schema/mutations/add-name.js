const {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt
} = require("graphql");

const pgdb = require("../../database/pgdb");

const Name = require("../types/name");

const NameInput = new GraphQLInputObjectType({
  name: "NameInput",

  fields: {
    apiKey: { type: new GraphQLNonNull(GraphQLString) },
    contestId: { type: new GraphQLNonNull(GraphQLInt) },
    label: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString }
  }
});

module.exports = {
  type: Name,
  args: {
    input: { type: new GraphQLNonNull(NameInput) }
  },
  resolve(obj, { input }, { pgPool }) {
    return pgdb(pgPool).addName(input);
  }
};
