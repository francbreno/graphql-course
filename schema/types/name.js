const {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLID,
  GraphQLString
} = require("graphql");

// const pgdb = require("../../database/pgdb");

module.exports = new GraphQLObjectType({
  name: "Name",

  fields: () => {
    const User = require("./user");
    const TotalVotes = require("./total-votes");
    return {
      id: { type: GraphQLID },
      label: { type: GraphQLNonNull(GraphQLString) },
      description: { type: GraphQLString },
      createdAt: { type: GraphQLNonNull(GraphQLString) },
      createdBy: {
        type: GraphQLNonNull(User),
        resolve(obj, args, { loaders }) {
          // resolve(obj, args, { pgPool }) {
          // return pgdb(pgPool).getUserById(obj.createdBy);
          return loaders.usersByIds.load(obj.createdBy);
        }
      },
      totalVotes: {
        type: TotalVotes,
        resolve(obj, args, { loaders }) {
          return loaders.totalVotesByNameIds.load(obj.id);
        }
      }
    };
  }
});
