const { GraphQLUnionType } = require("graphql");

const Contest = require("./contest");
const Name = require("./name");

module.exports = new GraphQLUnionType({
  name: "Activity",

  types: [Contest, Name],
  resolveType(value) {
    return value.activityType === "contest" ? Contest : Name;
  }
});
