const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList
} = require("graphql");

// const pgdb = require('../../database/pgdb');
// const mdb = require("../../database/mdb");
// (1) User -> (n) Contests
module.exports = new GraphQLObjectType({
  name: "User",

  fields: () => {
    const Contest = require("./contest");
    const Activity = require("./activity");
    return {
      id: { type: GraphQLID },
      email: { type: new GraphQLNonNull(GraphQLString) },
      // firstName: { type: GraphQLString, resolve: obj => obj.first_name }
      firstName: { type: GraphQLString },
      lastName: { type: GraphQLString },
      fullName: {
        type: GraphQLString,
        resolve: obj => `${obj.firstName} ${obj.lastName}`
      },
      createdAt: { type: GraphQLString },
      contests: {
        type: new GraphQLList(Contest),
        // resolve(obj, args, { pgPool }) {
        //   return pgdb(pgPool).getContests(obj);
        resolve(obj, args, { loaders }) {
          return loaders.contestsForUserIds.load(obj.id);
        }
      },
      contestsCount: {
        type: GraphQLInt,
        // resolve(obj, args, { mongoPool }, { fieldName }) {
        //   return mdb(mongoPool).getCounts(obj, fieldName);
        resolve(obj, args, { loaders }, { fieldName }) {
          return loaders.mdb.usersByIds
            .load(obj.id)
            .then(res => res[fieldName]);
        }
      },
      namesCount: {
        type: GraphQLInt,
        // resolve(obj, args, { mongoPool }, { fieldName }) {
        //   return mdb(mongoPool).getCounts(obj, fieldName);
        resolve(obj, args, { loaders }, { fieldName }) {
          return loaders.mdb.usersByIds
            .load(obj.id)
            .then(res => res[fieldName]);
        }
      },
      votesCount: {
        type: GraphQLInt,
        // resolve(obj, args, { mongoPool }, { fieldName }) {
        //   return mdb(mongoPool).getCounts(obj, fieldName);
        resolve(obj, args, { loaders }, { fieldName }) {
          return loaders.mdb.usersByIds
            .load(obj.id)
            .then(res => res[fieldName]);
        }
      },
      activities: {
        type: new GraphQLList(Activity),
        resolve(obj, args, { loaders }) {
          return loaders.activitiesForUserIds.load(obj.id);
        }
      }
    };
  }
});
