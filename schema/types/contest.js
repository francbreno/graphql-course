const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLNonNull,
  GraphQLList
} = require("graphql");

// const pgdb = require('../../database/pgdb');

// (1) Contest -> (1) User
module.exports = new GraphQLObjectType({
  name: "Contest",

  fields: () => {
    const ContestStatus = require("./contest-status");
    const Name = require("./name");
    return {
      id: { type: GraphQLID },
      code: { type: new GraphQLNonNull(GraphQLString) },
      title: { type: new GraphQLNonNull(GraphQLString) },
      description: { type: GraphQLString },
      status: { type: new GraphQLNonNull(ContestStatus) },
      createdAt: { type: new GraphQLNonNull(GraphQLString) },
      names: {
        type: GraphQLList(Name),
        // resolve: (obj, args, { pgPool }) => {
        //   return pgdb(pgPool).getNames(obj);
        resolve: (obj, args, { loaders }) => {
          return loaders.namesForContestIds.load(obj.id);
        }
      }
    };
  }
});
